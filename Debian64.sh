#!/bin/sh

NAME="PeppermintOS-Classic"

###########################################################

while getopts "di" option; do
   case $option in
      d) # HDD image
            cp ./auto/hdd ./auto/config
            lb clean
            lb config
            lb build;;
      i) # ISO image
            cp ./auto/iso ./auto/config
            lb clean
            lb config
            lb build
            mv live-image-amd64.packages ${NAME}_x86-64.packages
            mv live-image-amd64.hybrid.iso ${NAME}_x86-64.iso;;
     \?) # Invalid option
         echo "Error: Invalid option"
         exit;;
   esac
done
